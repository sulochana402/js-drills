const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.


*/

// 1. Get all items that are available
// using loops
function availableItems(data) {
  let itemsList = [];
  for (let item of data) {
    if (item.available === true) {
    itemsList.push(item);
    }
  }
  return itemsList
}
console.log(availableItems(items));


// using methods
function availableItems(data){
  const itemsList = data.filter((item)=>{
    if(item.available === true){
      return item;
    }
  });
  return itemsList;
}
console.log(availableItems(items));


// 2. Get all items containing only Vitamin C.
// using loops
function itemsContainingOnlyVitaminC(data){
  let newArray = [];
  for(let item of data){
    if(item.contains.split(',').length === 1){
      if(item.contains === "Vitamin C"){
        newArray.push(item);
      }
    }
  }
  return newArray;
}
console.log(itemsContainingOnlyVitaminC(items));


// using Methods 
function itemsContainingOnlyVitaminC(data){
  return data.filter((item)=>{
    if(item.contains === "Vitamin C"){
      return item;
    }
  })
}
console.log(itemsContainingOnlyVitaminC(items));



// 3. Get all items containing Vitamin A.
// using loops
function itemsContainingVitaminA(data){
  let newArray = [];
  for(let item of data){
    // console.log(item);
    for(let items of item.contains)
    if(items === "Vitamin A"){
      newArray.push(item);
    }
  }
  return newArray;
}
console.log(itemsContainingVitaminA(items));



// using methods
function itemsContainingVitaminA(data){
  return data.filter((item)=>{
    if(item.contains.includes("Vitamin A")){
      return item;
    }
  })
}
console.log(itemsContainingVitaminA(items));




// 4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }
function groupedItems(data){
  const vitamins = data.reduce((acc,item)=>{
    const splitted = item.contains.split(", ");
    // console.log(splitted);
    splitted.forEach(vitamin => {
      if(!acc[vitamin]){
        acc[vitamin] = [];
      }
      acc[vitamin].push(item.name);
    });
    return acc;
  },[]);
  return vitamins;
}
console.log(groupedItems(items));


// 5. Sort items based on number of Vitamins they contain.
// using methods
function sortedItems(data){
  const listItems = data.sort((vitaminA,vitaminB)=>{
    let first = vitaminA.contains.split(',');
    let second = vitaminB.contains.split(',');
    return first.length - second.length;
  });
  return listItems;
}
console.log(sortedItems(items));


// using bubble sort
function sortedItems(data){
    
  const countVitamins = item => item.contains.split(', ').length;
  
  for (let i = 0; i < data.length - 1; i++) {
      for (let j = 0; j < data.length - i - 1; j++) {
          if (countVitamins(data[j]) > countVitamins(data[j + 1])) {
              [data[j], data[j + 1]] = [data[j + 1], data[j]];
          }
      }
  } 
  return data;
}
console.log(sortedItems(items));
