const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 
// Q1 Find all users who are interested in playing video games.

function usersInterestedInVideoGames(data){
    let userDetails = [];
    const userInterest = Object.values(data).filter((user)=>{
        //   console.log((user.interests));
        user.interests.forEach(element => {
            if(element.includes("Video Games")){
               return userDetails.push(user);
            }
        });
    });
    return userDetails;
}
console.log(usersInterestedInVideoGames(users));



// Q2 Find all users staying in Germany.
function usersStayingInGermany(data){
    const germanyUsers = Object.entries(data).filter(([,user])=>{
        if(user.nationality === "Germany"){
            return user;
        }
    }).map(([name,user])=> ({name, ...user}));
    return germanyUsers;
}
console.log(usersStayingInGermany(users));





// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10
function sortUsersSeniorityLevel(data) {
    const designationOrder = {
        "Senior": 3,
        "Developer": 2,
        "Intern": 1
    };
    return Object.values(data).sort((userA, userB) => {
        // Compare seniority levels based on designation
        const seniorityComparison = designationOrder[userB.designation] - designationOrder[userA.designation];
        if (seniorityComparison !== 0) {
            return seniorityComparison;
        }
        return userB.age - userA.age;
    });
}
console.log(sortUsersSeniorityLevel(users));




// Q4 Find all users with masters Degree.
function usersWithMastersDegree(data){
    const mastersDegree = Object.entries(data).filter(([,user])=>{
        if(user.qualification === "Masters"){
            return user;
        }
    }).map(([name,user])=> ({name, ...user}));
    return mastersDegree;
}
console.log(usersWithMastersDegree(users));



// Q5 Group users based on their Programming language mentioned in their designation.
function usersDesignation(data){
    const groupedUsers = Object.entries(data).reduce((acc,[userId,user])=>{
        const language =  user.desgination.split(' ')[0];
        if(!acc[language]) {
            acc[language] = [];
        }
        acc[language].push(userId);
        return acc;
    },[])
    return groupedUsers;
}
console.log(usersDesignation(users));